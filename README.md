# SQLite3 to Postgres13

This docker image provides the tool `sequel` which will help
to transfer the SQLite data to a postgres instance.

See also https://stackoverflow.com/questions/34680812/how-to-migrate-from-sqlite3-to-postgresql

## How to

1. Start the docker-compose service
2. Wait until the database is initialized
3. Connect to the docker container
4. Execute the following command

```
sequel -C sqlite:///home/db.sqlite3 postgres://paperless:paperless@localhost/paperless
```
