FROM postgres:13

RUN apt update && apt install -y ruby-full ubuntu-dev-tools libsqlite3-dev libpq-dev

RUN gem install sqlite3 pg sequel

VOLUME [/var/lib/postgresql/data]
ENTRYPOINT ["docker-entrypoint.sh"]
EXPOSE 5432
CMD ["postgres"]

